<?php

/**
 * @file
 * Contains \Drupal\block_page\Form\PageVariantFormBase.
 */

namespace Drupal\block_page\Form;

use Drupal\block_page\BlockPageInterface;
use Drupal\Core\Form\FormBase;

/**
 * Provides a base form for editing and adding a page variant.
 */
abstract class PageVariantFormBase extends FormBase {

  /**
   * The block page this page variant belongs to.
   *
   * @var \Drupal\block_page\BlockPageInterface
   */
  protected $blockPage;

  /**
   * The page variant used by this form.
   *
   * @var \Drupal\block_page\Plugin\PageVariantInterface
   */
  protected $pageVariant;

  /**
   * Prepares the page variant used by this form.
   *
   * @param string $page_variant_id
   *   Either a page variant ID, or the plugin ID used to create a new variant.
   *
   * @return \Drupal\block_page\Plugin\PageVariantInterface
   */
  abstract protected function preparePageVariant($page_variant_id);

  /**
   * @return string
   */
  abstract protected function submitText();

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state, BlockPageInterface $block_page = NULL, $page_variant_id = NULL) {
    $this->blockPage = $block_page;
    $this->pageVariant = $this->preparePageVariant($page_variant_id);

    // Allow the page variant to add to the form.
    $form['plugin'] = $this->pageVariant->buildConfigurationForm(array(), $form_state);
    $form['plugin']['#tree'] = TRUE;

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->submitText(),
      '#button_type' => 'primary',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, array &$form_state) {
    // Allow the page variant to validate the form.
    $plugin_values = array(
      'values' => &$form_state['values']['plugin']
    );
    $form['plugin'] = $this->pageVariant->validateConfigurationForm($form, $plugin_values);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    // Allow the page variant to submit the form.
    $plugin_values = array(
      'values' => &$form_state['values']['plugin']
    );
    $this->pageVariant->submitConfigurationForm($form, $plugin_values);
  }

}
